## The Warehouse Group
Retail is a fast-paced industry to work in; we live and breathe to serve our customers and reach our targets by being at the forefront of our industry and this role is critical to achieving this.

Our team of software developers is responsible for the implementation of beautiful, elegant and clean software solutions that meet business objectives.

## Objective

Your objective is to create a simple online shop where a visitor can:

- Browse all products by category.
- Search all products by keyword.
- Add products to a shopping cart.
- View an overview of products in their shopping cart with a total price, on a page labeled "Shopping Cart".

#### Minimum Requirements

The requirements are simple, but you can use your imagination to provide more features than listed here.

- No more than 12 products should display on a catalog/search page at a time.
- Catalog/search pages should display a product's name, price, and image.
- Search should include title and brand.
- Shopping cart should persist for 48 hours after the browser has closed.
- Mobile-first design.
- Relevant Unit tests for all backend code.
- Code Documentation

``Please give extra attention to the front-end if you are applying for a front-end role.``

### Suggested Frontend Stack

- jQuery 3+
- SCSS
- Bootstrap 4
- Font Awesome 4

#### Optional

- SPA in Vue.js / React
- Webpack

### Suggested Backend Stack

- Node.js LTS
- Express
- Chai.js / Mocha

## Resources

Product Data feed:  
https://bitbucket.org/twgnz/twg-public-ecomm-code-challenge/src/master/noelleeming-catalog.json

Hot-linking of images may be blocked, be creative. 

Feel free to use design ideas, including logos, images and colors from our existing website: https://www.noelleeming.co.nz

## Evaluation

You will be evaluated not only by the functional requirements, but mainly for the quality of your code. 

- Test coverage
- Modularity
- Simplicity
- Reusability
- Readability 

Also by the speed, performance, and usability of your customer-facing code.

### Bonus Points

- While a fancy UI is not required; attention to detail should not be overlooked when giving customers appropriate prompts and feedback on their actions. In short; you need to care about how a customer will interact with your site.
- While jQuery is suggested, use of Vanilla JavaScript where appropriate is desired.
- Good commit messages
- Public cloud deployment
- Usage of the ESLint Airbnb JavaScript Style Guide: http://airbnb.io/javascript/css-in-javascript/
- Good CSS/SCSS structure/architecture


### Instructions:

* Clone this repository: https://bitbucket.org/twgnz/twg-public-ecomm-code-challenge
* You have 7 days after receiving the challenge to push your code and share the repository with us.
* Happy coding!